package com.example.abood.feedback

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.hsalf.smilerating.SmileRating
import com.hsalf.smilerating.BaseRating
import com.hsalf.smilerating.BaseRating.Smiley
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
val IMAGE_CAPTURE=100
        mCameraBut.setOnClickListener {
            var i=Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(i,IMAGE_CAPTURE)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
if(requestCode==100){
    var bmp=data?.extras?.get("data")as Bitmap
    mImageView.setImageBitmap(bmp)
}

    }

    }


